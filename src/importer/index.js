import parse from 'csv-parse';
import parseSync from 'csv-parse/lib/sync';
import { readFileSync, createReadStream } from 'fs';
export class Importer {
    import(path) {
        const list = [];
        return new Promise((resolve, reject) => {
            const readableStream = createReadStream(path).pipe(parse({ columns: true }));
            readableStream.on('readable', () => {
                let chunk;
                while (null !== (chunk = readableStream.read())) {
                    list.push(chunk);
                }
                resolve(list);
            });
        });
    }
    importSync(path) {
        const data = readFileSync(path, 'utf8');
        return parseSync(data, { columns: true });
    }
}
//# sourceMappingURL=index.js.map