import appConfig from './config/config';
import { User, Product } from './models';

import { DirWatcher, DIRWATCHER_CHANGED_EVENT } from './dirwatcher';
import { Importer } from './importer';

let user = new User('user 1');
let product = new Product('product 1');

console.log(appConfig.name);
console.log(user.name);
console.log(product.name);

let dirWatcher = new DirWatcher();
let importer = new Importer();

dirWatcher.watch(__dirname + '/../data', 1000);

dirWatcher.on(DIRWATCHER_CHANGED_EVENT, (file) => {
    console.log('Change detected', file);
    if (file.fileName.endsWith('.csv')) {
        importer.import(file.filePath).then((data) => {
            console.log(data);
            const data2 = importer.importSync(file.filePath);
            console.log(data2);
        });
    }
});

