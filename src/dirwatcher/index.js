import { readdir, readFile, lstat } from 'fs';
import { resolve } from 'path';
import { createHash } from 'crypto';
import { EventEmitter } from 'events';
import { promisify } from "util";
// convert callback to promise
// http://2ality.com/2017/05/util-promisify.html
const readDirAsync = promisify(readdir);
const readFileAsync = promisify(readFile);
const lstatAsync = promisify(lstat);
export const DIRWATCHER_CHANGED_EVENT = 'dirwatcher:changed';
export class DirWatcher extends EventEmitter {
    constructor(path, delay) {
        super();
        this.path = path;
        this.delay = delay;
        this.fileList = {};
        this.watch = (path, delay) => {
            this.path = path || this.path;
            this.delay = delay || this.delay || 1000;
            this.stopTimer();
            this.readDir();
        };
        this.stopTimer = () => {
            if (this.timer) {
                clearTimeout(this.timer);
                this.timer = undefined;
            }
        };
        this.scheduleNextRun = () => {
            this.timer = setTimeout(this.readDir, this.delay);
        };
        this.readDir = () => {
            readDirAsync(this.path)
                .then(this.readDirCallback);
        };
        this.readDirCallback = (files) => {
            return Promise.all(files.map((fileName) => {
                const filePath = resolve(this.path, fileName);
                return lstatAsync(filePath)
                    .then((stats) => {
                    if (stats.isFile()) {
                        return readFileAsync(filePath);
                    }
                    else {
                        return Promise.reject('not a file');
                    }
                })
                    .then((data) => {
                    const hash = createHash('md5').update(data).digest("hex");
                    const newFile = !this.fileList[fileName];
                    if (newFile || this.fileList[fileName] !== hash) {
                        const eventArgument = {
                            fileName,
                            filePath,
                            hash,
                            data,
                            newFile
                        };
                        this.emit(DIRWATCHER_CHANGED_EVENT, eventArgument);
                        // store state
                        this.fileList[fileName] = hash;
                        return eventArgument;
                    }
                })
                    .catch((err) => {
                    // console.error(err);
                    return Promise.resolve();
                });
            })).then(this.scheduleNextRun);
        };
    }
}
//# sourceMappingURL=index.js.map