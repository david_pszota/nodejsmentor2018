import {readdir, readFile, lstat, Stats} from 'fs';
import {resolve} from 'path';
import {createHash} from 'crypto';
import {EventEmitter} from 'events';
import {promisify} from "util";

// convert callback to promise
// http://2ality.com/2017/05/util-promisify.html
const readDirAsync = promisify(readdir);
const readFileAsync = promisify(readFile);
const lstatAsync = promisify(lstat);

export interface IDirWatcherChangedEventArgument {
    fileName: string;
    filePath: string;
    hash: string;
    data: Buffer;
    newFile: boolean;
}

export const DIRWATCHER_CHANGED_EVENT = 'dirwatcher:changed';

export class DirWatcher extends EventEmitter {

    private timer: NodeJS.Timer;
    private readonly fileList: { [fileName: string]: string } = {};

    constructor(
        public path?: string,
        public delay?: number
    ) {
        super();
    }

    public watch = (path?: string, delay?: number) => {
        this.path = path || this.path;
        this.delay = delay || this.delay || 1000;
        this.stopTimer();
        this.readDir();
    };

    public stopTimer = () => {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = undefined;
        }
    };

    private scheduleNextRun = () => {
        this.timer = setTimeout(this.readDir, this.delay);
    };

    private readDir = () => {
        readDirAsync(this.path)
            .then(this.readDirCallback)
    };

    private readDirCallback = (files: string[]): Promise<any> => {
        return Promise.all(files.map((fileName: string) => {
            const filePath = resolve(this.path, fileName);
            return lstatAsync(filePath)
                .then((stats: Stats) => {
                    if (stats.isFile()) {
                        return readFileAsync(filePath);
                    } else {
                        return Promise.reject('not a file');
                    }
                })
                .then((data: Buffer) => {
                    const hash = createHash('md5').update(data).digest("hex");
                    const newFile = !this.fileList[fileName];
                    if (newFile || this.fileList[fileName] !== hash) {
                        const eventArgument: IDirWatcherChangedEventArgument = {
                            fileName,
                            filePath,
                            hash,
                            data,
                            newFile
                        };
                        this.emit(DIRWATCHER_CHANGED_EVENT, eventArgument);
                        // store state
                        this.fileList[fileName] = hash;
                        return eventArgument;
                    }
                })
                .catch((err) => {
                    // console.error(err);
                    return Promise.resolve();
                });
        })).then(this.scheduleNextRun)
    }
}

